import React from "react";
import { Route, BrowserRouter as Router } from "react-router-dom";
import "./App.css";
import Header from "./Header";
import Nav from "./Nav";
import Home from "./Home";
import Contact from "./Contact";
import About from "./About";
import Footer from "./Footer";

function App() {
  return (
    <div className="App">
      <Router basename={"/build"}>
        <Header />
        <Nav />
        <Route exact path={`/home`} component={Home} />
        <Route path={`/Contact`} component={Contact} />
        <Route path={`/about`} component={About} />
        <Footer />
      </Router>
    </div>
  );
}

export default App;
