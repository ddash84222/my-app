import React, { Component } from 'react'
import { Link } from "react-router-dom";

export default class nav extends Component {
    test(event){
        document.getElementById('test2').classList.remove('show');
    }
    test2(event){
        document.getElementById('test2').classList.add('show');
    }
    render() {
        return (
            <div>
            <div className="container">
    <div className="row">
        <div className="col-md-4">
            <div className="logo">
                <Link to="/">Logo</Link>
            </div>
        </div>
        <div className="col-md-8">
           
            <ul className="navbar-nav2 dropdown" id="test">
                <li><Link to="/home">Home</Link></li>
                <li onMouseOut={() => { document.getElementById('test2').classList.remove('show'); } } onMouseOver={ (event) => {this.test2(event);} }><Link to="/home">Home</Link>
               
                    <div id="test2" class="dropdown-content">
                    <Link onClick={ (event) => { this.test(); } } to="/home">Home</Link>
                    <Link onClick={ (event) => { this.test(); } } to="/about">About</Link>
                    <Link onClick={ (event) => { this.test(); } } to="/contact">Contact</Link>    
                   </div>
                </li>
                
                <li><Link to="/about">About</Link></li>
                <li><Link to="/contact">Contact</Link></li>
                    
                    
            </ul>
        </div>
    </div>
</div>

            </div>
            
        )
    }
}
